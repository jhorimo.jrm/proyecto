<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type"
          content="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inventario</title>
    <link rel="stylesheet" href="{{ asset('css/report.css') }}" />
</head>
<body>
<table style="width: 100%">
    <tr>
        <td colspan="13" class="title"><strong>Reporte Inventario</strong></td>
    </tr>
    <tr>
        <td colspan="2" class="label">Empresa:</td>
        <td>{{$company->name}}</td>
    </tr>
    <tr>
        <td colspan="2" class="label">RUC:</td>
        <td align="left">{{$company->number}}</td>
    </tr>
    <tr>
        <td colspan="2" class="label">Establecimiento:</td>
        <td>{{$establishment->address}} - {{$establishment->department->description}}
            - {{$establishment->district->description}}</td>
    </tr>
    <tr>
        <td colspan="2" class="label">Fecha:</td>
        <td>{{ date('d/m/Y')}}</td>
    </tr>
</table>
<table style="width: 100%" class="table-records">
    <thead>
    <tr>
        <th><strong>#</strong></th>
        <th><strong>Cod. de barras</strong></th>
        <th><strong>Cod. Interno</strong></th>
        <th><strong>Descripción</strong></th>
        <th><strong>Categoria</strong></th>
        <th align="right"><strong>Stock mínimo</strong></th>
        <th align="right"><strong>Stock actual</strong></th>
        <th align="right"><strong>Costo</strong></th>
        <th align="right"><strong>Costo Total</strong></th>
        <th align="right"><strong>Precio de venta</strong></th>
        <th><strong>Marca</strong></th>
        <th><strong>F. vencimiento</strong></th>
        <th><strong>Almacén</strong></th>
    </tr>
    </thead>
    <tbody>
    @php
        $total = 0;
    @endphp
    @foreach($records as $key => $row)
        @php
            $total_line = $row['stock'] * $row['purchase_unit_price'];
            $total = $total + $total_line;
        @endphp
        <tr>
            <td>{{ $loop->iteration}}</td>
            <td>{{ $row['barcode'] }}</td>
            <td>{{ $row['internal_id'] }}</td>
            <td>{{ $row['name'] }}</td>
            <td>{{ $row['item_category_name'] }}</td>
            <td align="right">{{ $row['stock_min'] }}</td>
            <td align="right">{{ $row['stock'] }}</td>
            <td align="right">{{ $row['purchase_unit_price'] }}</td>
            <td align="right">{{ $total_line }}</td>
            <td align="right">{{ $row['sale_unit_price'] }}</td>
            <td>{{ $row['brand_name'] }}</td>
            <td>{{ $row['date_of_due'] }}</td>
            <td>{{ $row['warehouse_name'] }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="8" align="right">Costo Total de Inventario</td>
        <td align="right">{{ $total }}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
</body>
</html>
